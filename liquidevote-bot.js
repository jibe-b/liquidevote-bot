var fs = require('fs');
var config = require('./config.js');

function voteLiquid () {
	var gitlab_api_key = getTheApiKey();

	return "Liquid vote may be the future of vote.";
}

function getTheApiKey () {
	console.log(config.gitlab_api_key_file);
	var gitlab_api_key = fs.readFileSync(config.gitlab_api_key_file).toString();

	return gitlab_api_key;
}

module.exports = {voteLiquid, getTheApiKey};