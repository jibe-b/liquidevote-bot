# 2. Implement the code with a Test Driven Development approach

Date: 2018-06-04

## Status

Accepted

## Context

Testing code from the beginning keeping code clean and in the radar.

## Decision

Test Driven Development best pratices MUST be followed.

## Consequences

The code should stay under control.
