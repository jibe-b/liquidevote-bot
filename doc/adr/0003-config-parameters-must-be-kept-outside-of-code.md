# 3. Config parameters MUST be kept outside of code

Date: 2018-06-04

## Status

Accepted

## Context

Parameters may vary when code should not.

## Decision

All parameters MUST be kept outside of code.

## Consequences

The git log will not evolve if parameters change.
