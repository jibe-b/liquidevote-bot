var assert = require('assert');

var voteLiquid = require('../liquidevote-bot.js')

describe("The app", function (){
	it("returns something", function(){
		//acteurs
		var expected_min = 1;

		//action
		var result = voteLiquid.voteLiquid().length;

		//test
		assert(expected_min <= result);
	});


describe("The backend", function () {
	it("has access to the Gitlab API key", function () {
		var expected_min = 1;

		var result = voteLiquid.getTheApiKey().length;

		assert(expected_min <= result);
	});
})


});